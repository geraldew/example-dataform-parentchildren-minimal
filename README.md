# Python Example Data Form Parent Children Minimal

An example of a Parent-Children Data Form

The idea here is to show a minimal-imports method of displaying parent-child records using stock Tkinter controls.

## Screenshot

![screenshot](Screenshot.png)

## Notes

It is assumed that there will be an SQLite database file, containing two tables with a column in each as the parent-child link.

For simplicity, the names thus required have hard-coded into four functions:
- ExampleDetail_Database_Filename
- ExampleDetail_Parent_Tablename
- ExampleDetail_Children_Tablename
- ExampleDetail_Parent_LinkColumnName
- ExampleDetail_Children_LinkColumnName
 
So to try this example with your own SQLite database file, just replace the values in those functions.

## Operation

With the five name functions properly set, the program will load both tables for viewing. Do note that this is just done within memory and without any filtering or paging - so maybe don't aim it at a table of a billion rows!

You can then click on a row in the parent table and click the `Enlink` button to have the child table reloaded with only matching rows.

If you turn on the Auto check box, then the child table is refreshed whenever a parent row is clicked. 
  
## Credit

While the code here is my own, the idea of making it came from watching a [YouTube video](https://www.youtube.com/watch?v=GKk-PBZKl1o) by Data Analytics Ireland. From there you can access both their source code and the SQLite script for making the datbase shown in the screenshot. My thanks to them for that.
