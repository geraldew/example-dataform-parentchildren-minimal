#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Python Example tkinter Application One Window Data Form Parent Child Minimal
#
# --------------------------------------------------
# Copyright (C) 2021-2021  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

import tkinter as im_tkntr 
import tkinter.ttk as im_tkntr_ttk
from tkinter import scrolledtext as im_tkntr_tkst

import sqlite3
from sqlite3 import Error

# --------------------------------------------------
# Variable naming
# For easy reading, this source code uses a simple set of prefixes for variable names:
# p_  is for parameters being passed in to a function
# i_  is for variables internal to a function
# r_  is for variables that will be returned by the function
# --------------------------------------------------

# --------------------------------------------------
# SQLite Wrappers
# --------------------------------------------------

def Sqlite_Connection_Close( p_conn):
	r_didok = False
	try:
		if p_conn:
			p_conn.close()
			r_didok = True
	except Error as i_e:
		print("SQLite Error: " + str( i_e) )
	return r_didok

def Sqlite_Connection_Create_ToFile( p_db_file):
	r_conn = None
	r_didok = False
	try:
		r_conn = sqlite3.connect( p_db_file)
		r_didok = True
	except Error as i_e:
		print("SQLite Error: " + str( i_e) )
	return r_didok, r_conn

def Sqlite_Sql_FetchRecords_WithColumnNames( p_conn, p_str_sql):
	r_didok = False
	try:
		c = p_conn.cursor()
		c.execute( p_str_sql )
		r_rows = c.fetchall()
		r_didok = True
	except Error as e:
		print("SQLite Error: " + str(e) )
		r_rows = []
		r_names = []
	if r_didok :
		r_names = list( map( lambda x: x[0], c.description) )
	return r_didok, r_rows, r_names

# --------------

def SqLite_Filename_GetTable_Rows( p_fn, p_tblnm ):
	r_didok = False
	r_rows = []
	r_names = []
	r_didok, i_conn = Sqlite_Connection_Create_ToFile( p_fn)
	if r_didok :
		i_str_sql = "SELECT * FROM " + p_tblnm + " ;"
		r_didok, r_rows, r_names = Sqlite_Sql_FetchRecords_WithColumnNames( i_conn, i_str_sql)
		Sqlite_Connection_Close( i_conn)
	return r_didok, r_rows, r_names

def SqLite_Filename_GetTable_Rows_Where_ForeignKey_IsValue( p_fn, p_tblnm, p_fkc, p_kval ):
	r_didok = False
	r_rows = []
	r_names = []
	r_didok, i_conn = Sqlite_Connection_Create_ToFile( p_fn)
	if r_didok :
		i_str_sql = "SELECT * FROM " + p_tblnm + " WHERE " + p_fkc + " = " + str(p_kval) + " ;"
		r_didok, r_rows, r_names = Sqlite_Sql_FetchRecords_WithColumnNames( i_conn, i_str_sql)
		Sqlite_Connection_Close( i_conn)
	return r_didok, r_rows, r_names

# --------------------------------------------------
# Interactive
# --------------------------------------------------

def ApplicationWindow( p_window_context ) : 
	# ------------
	# Hard coded values - done here as functions to make adapting easier and avoid variable overwrite issues
	# - Datbase file name
	def ExampleDetail_Database_Filename():
		return "/home/ger/tkinter_dbase.db"
	# - Parent table name
	def ExampleDetail_Parent_Tablename():
		return "POST_TYPE"
	# - Child table name
	def ExampleDetail_Children_Tablename():
		return "LEDGER_POSTING"
	# - Parent link column name
	def ExampleDetail_Parent_LinkColumnName():
		return "LEDGER_TYPE_NO"
	# - Child link column name
	def ExampleDetail_Children_LinkColumnName():
		return "LEDGER_TYPE_NO"
	# ------------
	# Feature functions
	def ClearTreeParent():
		# clear the child tree control
		for i in tk_trvw_prnt.get_children():
			tk_trvw_prnt.delete(i)
	def ClearTreeChildren():
		# clear the child tree control
		for i in tk_trvw_chld.get_children():
			tk_trvw_chld.delete(i)
	def GetCurrentParentKey():
		slctn_prnt = tk_trvw_prnt.selection()
		#print( slctn_prnt )
		curItem = tk_trvw_prnt.focus()
		#print( curItem )
		curParts = tk_trvw_prnt.item( curItem)
		#print( curParts )
		curRow = curParts[ "values"]
		#print( curRow )
		clms_idx_pky = tk_trvw_prnt["column"].index(ExampleDetail_Parent_LinkColumnName())
		#print( clms_idx_pky )
		pkyval = curRow[ clms_idx_pky ]
		#print( pkyval )
		return pkyval
	def Load_Tables_Initial() :
		ClearTreeParent()
		ClearTreeChildren()
		# parent table
		a_didok, a_rows, a_names = SqLite_Filename_GetTable_Rows( ExampleDetail_Database_Filename(), ExampleDetail_Parent_Tablename() )
		tk_trvw_prnt["column"] = a_names
		tk_trvw_prnt["show"] = "headings"
		for column in tk_trvw_prnt["columns"]:
			tk_trvw_prnt.heading(column, text=column)
		for row in a_rows:
			tk_trvw_prnt.insert("", "end", values=row)
		# child table
		b_didok, b_rows, b_names = SqLite_Filename_GetTable_Rows( ExampleDetail_Database_Filename(), ExampleDetail_Children_Tablename() )
		tk_trvw_chld["column"] = b_names
		tk_trvw_chld["show"] = "headings"
		for column in tk_trvw_chld["columns"]:
			tk_trvw_chld.heading(column, text=column)
		for row in b_rows:
			tk_trvw_chld.insert("", "end", values=row)
	# ------------
	# GUI interaction defs - these are the actions assigned to Tkinter controls
	def Cmd_button_exit() :
		p_window_context.quit()
	def Cmd_trvw_prnt_on_tree_select( p_event ) :
		if tk_chkvr_autolink.get() == 1 :
			Cmd_button_prnt_link()
	def Cmd_button_prnt_link() :
		ClearTreeChildren()
		# get the link column value from selected row
		p_kval = GetCurrentParentKey()
		# reload the child table with filter
		p_fn = ExampleDetail_Database_Filename()
		p_tblnm =  ExampleDetail_Children_Tablename()
		p_fkc = ExampleDetail_Children_LinkColumnName()
		b_didok, b_rows, b_names = SqLite_Filename_GetTable_Rows_Where_ForeignKey_IsValue( p_fn, p_tblnm, p_fkc, p_kval )
		tk_trvw_chld["column"] = b_names
		tk_trvw_chld["show"] = "headings"
		for column in tk_trvw_chld["columns"]:
			tk_trvw_chld.heading(column, text=column)
		for row in b_rows:
			tk_trvw_chld.insert("", "end", values=row)
	# -------------------------------------------------
	# Now we have the "main" code for ApplicationWindow in which the Tkinter controls get defined
	# The placements here are all done using the "pack" approach with frames used to organise them
	# --------- 
	tk_frame_top = im_tkntr.Frame( p_window_context)
	tk_frame_top.pack( fill = im_tkntr.X)
	# make frame content
	tk_button_exit = im_tkntr.Button( tk_frame_top, text="(X)", command=Cmd_button_exit)
	tk_label_title = im_tkntr.Label( tk_frame_top, text="v0.1" )
	tk_label_explain = im_tkntr.Label( tk_frame_top, text="A trial of Parent-Children Data Forms")
	# place the content
	tk_button_exit.pack( side=im_tkntr.LEFT, padx=5, pady=5)
	tk_label_title.pack( side = im_tkntr.LEFT, padx=5, pady=5)
	tk_label_explain.pack( side = im_tkntr.RIGHT, padx=5, pady=5)
	# ----------
	# Parent LabelFrame
	tk_lblframe_parent = im_tkntr.LabelFrame( p_window_context, text="Parent records")
	tk_lblframe_parent.pack( fill="both", expand="yes" ) 
	# --------- SubFrame 1 Parent Data
	tk_frame_prnt_data = im_tkntr.Frame( tk_lblframe_parent)
	tk_frame_prnt_data.pack( fill="both", expand="yes")
	# Parent records Treeview, scrollbars
	tk_trvw_prnt = im_tkntr_ttk.Treeview( tk_frame_prnt_data)
	tk_trvw_prnt.place( relheight=1.0, relwidth=1.0)
	# adding scrollbars
	tk_trvw_prnt_treescrolly = im_tkntr.Scrollbar(tk_frame_prnt_data, orient="vertical", command=tk_trvw_prnt.yview)
	tk_trvw_prnt_treescrollx = im_tkntr.Scrollbar(tk_frame_prnt_data, orient="horizontal", command=tk_trvw_prnt.xview)
	tk_trvw_prnt.configure( xscrollcommand=tk_trvw_prnt_treescrollx.set, yscrollcommand=tk_trvw_prnt_treescrolly.set)
	tk_trvw_prnt_treescrollx.pack(side="bottom", fill="x")
	tk_trvw_prnt_treescrolly.pack(side="right", fill="y")
	tk_trvw_prnt.bind("<<TreeviewSelect>>", Cmd_trvw_prnt_on_tree_select)
	# Parent data control buttons
	# ---------
	tk_frame_p_ctrls = im_tkntr.Frame( tk_lblframe_parent)
	tk_frame_p_ctrls.pack( fill = im_tkntr.X)
	# Parent-Child Enlink button
	tk_button_prnt_link = im_tkntr.Button( tk_frame_p_ctrls, text="Enlink", command=Cmd_button_prnt_link)
	tk_button_prnt_link.pack( side=im_tkntr.LEFT, padx=5, pady=5)
	# Parent-Child auto Enlink checkbox
	tk_chkvr_autolink = im_tkntr.IntVar()
	tk_chkvr_autolink.set(0)
	tk_chkbx_autolink = im_tkntr.Checkbutton( tk_frame_p_ctrls, text="Auto Link", variable=tk_chkvr_autolink )
	tk_chkbx_autolink.pack( side=im_tkntr.LEFT, anchor=im_tkntr.W, expand=im_tkntr.YES)
	#
	# Children LabelFrame
	tk_lblframe_child = im_tkntr.LabelFrame( p_window_context, text="Children records")
	tk_lblframe_child.pack( fill="both", expand="yes" ) 
	# Children records Treeview, scrollbars
	# --------- 
	tk_frame_chld_data = im_tkntr.Frame( tk_lblframe_child)
	tk_frame_chld_data.pack( fill="both", expand="yes")
	# Parent records Treeview, scrollbars
	tk_trvw_chld = im_tkntr_ttk.Treeview( tk_frame_chld_data)
	tk_trvw_chld.place( relheight=1.0, relwidth=1.0)
	# adding scrollbars
	tk_trvw_chld_treescrolly = im_tkntr.Scrollbar(tk_frame_chld_data, orient="vertical", command=tk_trvw_chld.yview)
	tk_trvw_chld_treescrollx = im_tkntr.Scrollbar(tk_frame_chld_data, orient="horizontal", command=tk_trvw_chld.xview)
	tk_trvw_chld.configure( xscrollcommand=tk_trvw_chld_treescrollx.set, yscrollcommand=tk_trvw_chld_treescrolly.set)
	tk_trvw_chld_treescrollx.pack(side="bottom", fill="x")
	tk_trvw_chld_treescrolly.pack(side="right", fill="y")
	# Parent data control buttons
	# --------- 
	tk_frame_p_ctrls = im_tkntr.Frame( tk_lblframe_child)
	tk_frame_p_ctrls.pack( fill = im_tkntr.X)
	# Edit button
	tk_button_chld_edit = im_tkntr.Button( tk_frame_p_ctrls, text="Ignore this button")
	tk_button_chld_edit.pack( side=im_tkntr.LEFT, padx=5, pady=5)
	#
	Load_Tables_Initial()

def main_GUI_Interact( ) :
	root = im_tkntr.Tk()
	root.title("Python Example tkinter Application One Window Data Form Parent Child Minimal")
	root.wm_geometry("780x500")
	ApplicationWindow( root )
	root.mainloop()

# main to execute only if run as a program
if __name__ == "__main__":
	main_GUI_Interact( )
